package org.dm.games.racing;

import org.dm.games.racing.model.Vehicle;
import org.dm.games.racing.model.VehicleFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by derekmordarski on 9/24/15.
 */
public class RaceTrackSimulator {

    private static final Logger log = Logger.getLogger("RaceTrackSimulator");
    private static final float TRACK_LENGTH = 5.0f;    // 10 miles
    private final TrackInfo trackInfo;
    private final int numberOfLaps;
    private List<Vehicle> vehicleList;
    private Map<Vehicle, Float> vehicleDistanceMap = new HashMap<>();

    // The ...args means it's a variable length array - same as String[] args - throwback from C
    public static void main(String ...args) {
        int numberOfVehicles = Integer.parseInt(args[0]);
        int numberOfLaps = Integer.parseInt(args[1]);
        RaceTrackSimulator raceTrack = new RaceTrackSimulator(numberOfVehicles, numberOfLaps);
        raceTrack.start();

    }


    public RaceTrackSimulator(int numberOfVehicles, int numberOfLaps) {

        this.trackInfo = new TrackInfo();
        this.numberOfLaps = numberOfLaps;

        initVehicles(numberOfVehicles);
    }

    public void start() {
        log.info("Light em up!");

        for (int i = 0; i < numberOfLaps; i++) {
            for (Vehicle vehicle : this.vehicleList) {
                float previousDistance = vehicleDistanceMap.get(vehicle);
                float distance = vehicle.move(this.trackInfo);

                float total = previousDistance + distance;
                log.info(String.format("%s moved: %s miles; total=%s", vehicle, distance, total));
                if (total > TRACK_LENGTH) {
                    log.info(String.format("%s is the WINNER!", vehicle));
                    return;
                }

                // This just replaces the value at the key (Vehicle) with the new total distance...
                // really cheesy usage of a Map which contains key/value pairs
                vehicleDistanceMap.put(vehicle, total);

            }
        }
    }

    private void initVehicles(int numberOfVehicles) {
        this.vehicleList = new ArrayList<>(numberOfVehicles);
        for (int i = 0; i < numberOfVehicles; i++) {
            Vehicle vehicle = VehicleFactory.create(i);
            this.vehicleDistanceMap.put(vehicle, 0.0f);
            this.vehicleList.add(vehicle);
        }
    }



}
