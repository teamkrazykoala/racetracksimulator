package org.dm.games.racing.model;

import org.dm.games.racing.TrackInfo;

/**
 * Created by derekmordarski on 9/24/15.
 */
public class Truck extends Automobile {

    @Override
    public float move(TrackInfo trackInfo) {
        float speed = getSpeedBetween(30, 40);
        float distance = speed / 60;
        return distance;
    }
}
