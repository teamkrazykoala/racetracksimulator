package org.dm.games.racing.model;

import java.util.Random;

/**
 * Created by derekmordarski on 9/24/15.
 */
public abstract class Automobile implements Vehicle {

    protected final Random rnd = new Random();
    private int number;

    protected float getSpeedBetween(int min, int max) {
        return rnd.nextInt((max - min) + 1) + min;
    }

    @Override
    public void setup(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("%s #%s", getClass().getSimpleName(), this.number);
    }
}
