package org.dm.games.racing.model;

import org.dm.games.racing.model.Vehicle;

import java.util.Random;

/**
 * Created by derekmordarski on 9/24/15.
 *
 * Follows the standard Factory design pattern
 *
 */
public class VehicleFactory {

    private static final Random rnd = new Random();

    public static Vehicle create(int number) {
        Vehicle vehicle = null;

        int type = rnd.nextInt(3);

        switch (type) {
            case 0:
                vehicle = new Car();
                break;

            case 1:
                vehicle = new SportsCar();
                break;

            case 2:
                vehicle = new Truck();
                break;
        }

        if (vehicle != null) {
            vehicle.setup(number);
        }

        return vehicle;
    }

}
