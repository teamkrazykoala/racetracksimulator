package org.dm.games.racing.model;

import org.dm.games.racing.TrackInfo;

/**
 * Created by derekmordarski on 9/24/15.
 */
public interface Vehicle {

    /**
     * Moved the car
     *
     * @return  float     The distance traveled in miles
     */
    public float move(TrackInfo trackInfo);


    public void setup(int number);

}
