package org.dm.games.racing.model;

import org.dm.games.racing.TrackInfo;

/**
 * Created by derekmordarski on 9/24/15.
 */
public class Car extends Automobile {


    @Override
    public float move(TrackInfo trackInfo) {
        float speed = getSpeedBetween(40, 60);
        float distance = speed / 60;
        return distance;
    }

}
