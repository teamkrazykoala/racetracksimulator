package org.dm.games.racing.model;

import org.dm.games.racing.TrackInfo;

/**
 * Created by derekmordarski on 9/24/15.
 */
public class SportsCar extends Car {

    @Override
    public float move(TrackInfo trackInfo) {

        return super.move(trackInfo) * 1.05f;
    }
}
